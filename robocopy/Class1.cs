﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace robocopy
{
    public class robocopy
    {
        public void backup(string source, string target)
        {
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.FileName = @"C:\WINDOWS\system32\Robocopy.exe";
            proc.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\"", source, target);
            proc.Start();

            proc.WaitForExit();
        }

         public void backup(string source, string target, string parameters)
        {
            Process proc = new Process();
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.FileName = @"C:\WINDOWS\system32\Robocopy.exe";
            proc.StartInfo.Arguments = string.Format("\"{0}\" \"{1}\" {2}", source, target, parameters);
            proc.Start();

            proc.WaitForExit();
        }
    }
}
