﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using robocopy;

namespace roboapplication
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            string source = string.Format(@"{0}\KissBackup\KissSettingsSource.txt",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            string target = string.Format(@"{0}\KissBackup\KissSettingsTarget.txt",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            if(File.Exists(source))
            {
                List<string> PathArray = new List<string>();


                PathArray.AddRange(File.ReadLines(source));

                foreach(string path in PathArray)
                {
                    listBox1.Items.Add(path);
                }
            }

            if (File.Exists(target))
            {

                textBox1.Text = File.ReadAllText(target);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdlg = new FolderBrowserDialog();
            DialogResult result = fdlg.ShowDialog();

            if(result == DialogResult.OK)
            {
                listBox1.Items.Add(fdlg.SelectedPath);
            }
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            List<string> PathArray = new List<string>();
            
            foreach(string path in listBox1.Items)
            {
                PathArray.Add(path);
            }

            String ApdataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string ApdataPathSource = string.Format(@"{0}\KissBackup\KissSettingsSource.txt",ApdataPath);
            System.IO.FileInfo file = new System.IO.FileInfo(ApdataPathSource);
            file.Directory.Create();
            System.IO.File.WriteAllLines(file.FullName,PathArray);

            
            string ApdataPathTarget = string.Format(@"{0}\KissBackup\KissSettingsTarget.txt", ApdataPath);
            System.IO.FileInfo file2 = new System.IO.FileInfo(ApdataPathTarget);
            file2.Directory.Create();
            System.IO.File.WriteAllText(file2.FullName, textBox1.Text);
            



        }

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Items.RemoveAt(listBox1.SelectedIndex);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdlg = new FolderBrowserDialog();
            DialogResult result = fdlg.ShowDialog();

            if (result == DialogResult.OK)
            {
                textBox1.Text = fdlg.SelectedPath;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach(string path in listBox1.Items)
            {
                robocopy.robocopy backup = new robocopy.robocopy();

                backup.backup(path, textBox1.Text, textBoxArg.Text);


            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
